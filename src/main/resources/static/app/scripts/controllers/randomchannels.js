'use strict';

/**
 * @ngdoc function
 * @name twitchableFrontEndApp.controller:RandomchannelsCtrl
 * @description
 * # RandomchannelsCtrl
 * Controller of the twitchableFrontEndApp
 */
angular.module('twitchableFrontEndApp')
  .controller('RandomchannelsCtrl', function (RandomChannels, LiveStream, $scope, $location) {

    this.randomchannels = RandomChannels.getData().query();
    $scope.setStreamChannel = function(channel){
    	LiveStream.setStreamChannel(channel);
    	$location.path("/video");
    }
  });
