'use strict';

/**
 * @ngdoc function
 * @name twitchableFrontEndApp.controller:ViewstreamCtrl
 * @description
 * # ViewstreamCtrl
 * Controller of the twitchableFrontEndApp
 */
angular.module('twitchableFrontEndApp')
  .controller('ViewstreamCtrl', function (LiveStream) {
	  
	  var channel = LiveStream.getStreamChannel();

    this.streamLink = LiveStream.getStreamLink();
    
    this.streamTitle = channel.status;
    
    this.channelName = channel.displayName;
    
    this.channelLink = channel.url + "/profile";
    
    this.channelLogo = channel.logo;
    
    this.numberFollowers = channel.followersNumber;
    
    this.numberViewers = channel.viewsNumber;
    
    this.rating = channel.rating;
    
    this.chatLink = channel.url + "/chat";


  });
