package com.twitchable.project.web;

import com.twitchable.project.model.Channel;
import com.twitchable.project.service.ChannelService;
import com.twitchable.project.service.UserService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;


/**
 * Created by riste on 4/7/2016.
 */

@Controller
public class HomeController {

    @Autowired
    UserService userService;

    @Autowired
    ChannelService channelService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model, HttpSession session) throws IOException, InterruptedException, JSONException {
        //allLinksChannels();
        List<Channel> channels = channelService.getAllChannels();
        model.addAttribute("channels",channels);
        return "index";
    }

    private StringBuilder fetchData(String link) throws InterruptedException, IOException {
        StringBuilder builder=new StringBuilder();
        //try{
        URL url = new URL(link);
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null){
            builder.append(inputLine);
            builder.append("\n");
            //ISKOMENTIRANIOV DEL KE SE DODADE NA STRANATA ZA AKO NE BLOKIRA API-to
        }
//                    System.out.println(builder.toString());
//                    System.out.println("LALALLALALALLALALA");
        //   parseJSON(builder);
//                } catch (Exception ex) {
//                    Thread.sleep(1000);

//                    getAllChannels(link);
//                }
        return builder;
    }

    private void allLinksChannels() throws JSONException, InterruptedException, IOException{
        boolean firstTime=true;
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for(char c : alphabet) {
            String nextLink = "https://api.twitch.tv/kraken/search/channels?limit=100&offset=0&q="+c;
            //List all Links
            //SEUSHTE NEMA STAVENO KRAEN USLOV ZASO NE DOJDOV DO TAKOV
            boolean flag = true;
            int i = 0;
            while (flag) {
                System.out.println("LINK:" + nextLink);
                //getAllLinks gets the page
                StringBuilder builder = fetchData(nextLink);
                //FULLPAGE JSON

                JSONObject fullPage = new JSONObject(builder.toString());
                parseJSONChannels(builder, fullPage);
                //flag = false;
                //GET THE NEXT LINK -krajniot uslov treba tyka da se definira
                nextLink = fullPage.getJSONObject("_links").get("next").toString();
                i++;
                if (i > 2) {
                    flag = false;
                }
            }
        }
    }

    private void parseJSONChannels(StringBuilder builder,JSONObject fullPage) throws JSONException {
        JSONArray allChannels=fullPage.getJSONArray("channels");
        for(int i=0;i<allChannels.length();i++){
            JSONObject channelObject=allChannels.getJSONObject(i);
            //RESTRICTIONS-dopolnitelno moze i da ima nad nekoj broj na followers
            if(!channelObject.get("followers").toString().equals("0")&&!channelObject.get("game").toString().equals("null")){
                Channel channel=createChannel(channelObject);
                if(channel.getFollowersNumber() > 50) {
                    channelService.addChannel(channel);
                }
            }
        }
    }

    private Channel createChannel(JSONObject channelObject) throws JSONException{
        String username = null;
        Channel channel = new Channel();
        if(channelObject.has("mature")) {
            channel.setMature(channelObject.getString("mature"));
        }
        if(channelObject.has("status")) {
            channel.setStatus(channelObject.getString("status"));
        }
        if(channelObject.has("broadcaster_language")) {
            channel.setBroadcasterLanguage(channelObject.getString("broadcaster_language"));
        }
        if(channelObject.has("display_name")) {
            channel.setDisplayName(channelObject.getString("display_name"));
        }
        if(channelObject.has("game")) {
            channel.setGame(channelObject.getString("game"));
        }
        if(channelObject.has("mature")) {
            channel.setMature(channelObject.getString("mature"));
        }
        if(channelObject.has("name")) {
            channel.setName(channelObject.getString("name"));
            username = channel.getName();
        }
        if(channelObject.has("createdAt")) {
            channel.setCreatedAt(channelObject.getString("createdAt"));
        }
        if(channelObject.has("updatedAt")) {
            channel.setUpdatedAt(channelObject.getString("updatedAt"));
        }
        if(channelObject.has("logo")) {
            channel.setLogo(channelObject.getString("logo"));
        }
        if(channelObject.has("url")) {
            channel.setUrl(channelObject.getString("url"));
        }
        if(channelObject.has("views")) {
            channel.setViewsNumber(Long.parseLong(channelObject.getString("views")));
        }
        if(channelObject.has("followers")) {
            channel.setFollowersNumber(Long.parseLong(channelObject.getString("followers")));
        }
        String streamUrl = "https://api.twitch.tv/kraken/streams/"+username;
        String liveStream = "https://player.twitch.tv/?channel="+username;
        String liveChat = "https://www.twitch.tv/"+username+"/chat?popout=";

        channel.setStreamUrl(streamUrl);
        channel.setLiveStream(liveStream);
        channel.setLiveChat(liveChat);

        /*channel.setRating(4.3);
        User u1 = new User();
        User u2 = new User();
        User u3 = new User();
        Rating r1 = new Rating();
        Rating r2 = new Rating();
        Rating r3 = new Rating();
        r1.setUser(u1);
        r2.setUser(u2);
        r3.setUser(u3);
        r1.setRating(5);
        r2.setRating(4);
        r3.setRating(4);
        List<Rating> list = new ArrayList<>();
        list.add(r1);
        list.add(r2);
        list.add(r3);*/
        return channel;
    }
}
