package com.twitchable.project.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AngularjsSpringController {

	@RequestMapping(value = "/randomchannels", method = RequestMethod.GET)
	public String randomChannelsView(Model m){
		return "randomchannels";
	}
	
	@RequestMapping(value = "/viewstream", method = RequestMethod.GET)
	public String streamView(Model m){
		return "viewstream";
	}
}
