package com.twitchable.project.rest;

import com.twitchable.project.model.Channel;
import com.twitchable.project.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Atanas on 17.04.2016.
 */
@RestController
@RequestMapping(value = "/channel")
@CrossOrigin
public class ChannelRest {

    @Autowired
    private ChannelService service;

    @RequestMapping(method = RequestMethod.GET)
    public List<Channel> findAll(){
        return service.getAllChannels();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Channel save(@RequestBody Channel channel){
        return service.addChannel(channel);
    }

    @RequestMapping(value = "/displayName/{name}", method = RequestMethod.GET)
    public Channel findByName(@PathVariable String name){
        return service.findChannelByName(name);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Channel findOne(@PathVariable String id){
        return service.findOne(id);
    }
}
