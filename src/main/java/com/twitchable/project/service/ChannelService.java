package com.twitchable.project.service;

import com.twitchable.project.model.Channel;
import com.twitchable.project.model.User;

import java.util.List;

/**
 * Created by riste on 4/7/2016.
 */
public interface ChannelService {
    public Channel addChannel(Channel c);
    public List<Channel> getAllChannels();
    public List<Channel> findChannelForUser(User u);
    public Channel findChannelByName(String display_name);
    public Channel findOne(String id);
    public void deleteAll();
}
