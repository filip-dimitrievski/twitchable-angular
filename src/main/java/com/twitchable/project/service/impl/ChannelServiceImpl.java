package com.twitchable.project.service.impl;

import com.twitchable.project.model.Channel;
import com.twitchable.project.model.User;
import com.twitchable.project.repository.ChannelRepository;
import com.twitchable.project.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by riste on 4/7/2016.
 */

@Service
public class ChannelServiceImpl implements ChannelService {

    @Autowired
    ChannelRepository channelRep;

    @Override
    public Channel addChannel(Channel c) {
        return channelRep.save(c);
    }

    @Override
    public List<Channel> getAllChannels() {
        return channelRep.findAll();
    }

    @Override
    public List<Channel> findChannelForUser(User u) {
        // tuka da se najdat site kanali na koj userot ima napraveno follow
        return null;
    }

    @Override
    public Channel findChannelByName(String display_name) {
        return channelRep.findByDisplayName(display_name);
    }

    @Override
    public Channel findOne(String id) {
        return channelRep.findOne(id);
    }

    @Override
    public void deleteAll() {
        channelRep.deleteAll();
    }
}
