package com.twitchable.project.service.impl;

import com.twitchable.project.model.User;
import com.twitchable.project.repository.UserRepository;
import com.twitchable.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by riste on 4/7/2016.
 */

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRep;

    @Override
    public void createUser(User u) {
        userRep.save(u);
    }

    @Override
    public List<User> getAllUsers() {
        return userRep.findAll();
    }

    @Override
    public User findUserByName(User u) {
        return userRep.findOne(u.getId());
    }

    @Override
    public void deleteAll() {
        userRep.deleteAll();
    }


}
