package com.twitchable.project.repository;

import com.twitchable.project.model.Channel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by riste on 4/7/2016.
 */

public interface ChannelRepository extends MongoRepository<Channel, String> {

    public Channel findByDisplayName(String displayName);
}
